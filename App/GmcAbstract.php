<?php
namespace M21\FeedGmc\App;

use \Magento\Framework\AppInterface as AppInterface;
use \Magento\Framework\App\Http as Http;

use Magento\Framework\ObjectManager\ConfigLoaderInterface;
use Magento\Framework\App\Request\Http as RequestHttp;
use Magento\Framework\App\Response\Http as ResponseHttp;
use Magento\Framework\Event;
use Magento\Framework\Filesystem;
use Magento\Framework\App\AreaList as AreaList;
use Magento\Framework\App\State as State;

abstract class GmcAbstract extends Http implements AppInterface
{
    const UPDATE_CRON_NORMAL_EXIT = 0;
    const UPDATE_CRON_EXIT_WITH_ERROR = 1;

    protected $state = null;
    protected $feed = null;
    protected $_response = null;

    public function __construct(
        Event\Manager $eventManager,
        AreaList $areaList,
        RequestHttp $request,
        ResponseHttp $response,
        ConfigLoaderInterface $configLoader,
        \Magento\Framework\App\State $state,
        \M21\FeedGmc\lib\Feed $feed
    )
    {

        $this->state = $state;
        $this->feed = $feed;
        $this->_response = $response;

        try {
            if (!$this->state->getAreaCode()) {
                $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML); // or \Magento\Framework\App\Area::AREA_FRONTEND
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function launch()
    {
        $this->run();
        return $this->_response;
    }

    abstract public function run();


    protected function _checkRespond($results)
    {
        if (!$results->ErrorMessage
            && $results->LoginResult == 'OK'
        ) {
            return true;
        } else {
            return false;
        }
    }

    public function catchException(\Magento\Framework\App\Bootstrap $bootstrap, \Exception $exception)
    {
        return false;
    }


}