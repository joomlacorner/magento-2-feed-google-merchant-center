<?php

namespace M21\FeedGmc\App;

use \M21\FeedGmc\App\GmcAbstract as GmcAbstract;

use Magento\Framework\ObjectManager\ConfigLoaderInterface;
use Magento\Framework\App\Request\Http as RequestHttp;
use Magento\Framework\App\Response\Http as ResponseHttp;
use Magento\Framework\Event;
use Magento\Framework\Filesystem;
use Magento\Framework\App\AreaList as AreaList;
use Magento\Framework\App\State as State;

class Gmc extends GmcAbstract
{

    protected $logger;

    /**
     * Execute the cron
     *
     * @return void
     */
    public function run()
    {
        try {
            if (!$this->state->getAreaCode()) {
                $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML); // or \Magento\Framework\App\Area::AREA_FRONTEND
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        // zwraca błąd że logger jest null
//        $this->logger->addInfo("Start");
        echo "START " . date('Y-m-d H:i:s') . "\n";
        $this->feed->run();
//        $this->logger->addInfo("Koniec");
        echo "KONIEC " . date('Y-m-d H:i:s') . "\n";
    }
}
